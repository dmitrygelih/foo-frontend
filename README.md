# vue-frontend

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Build docker image
```
docker build -t foo-app-frontend .
```

### Run docker image
```
docker run -it -p 8080:80 --rm --name foo-front foo-app-frontend:latest
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
