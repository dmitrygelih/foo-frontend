module.exports = {
  presets: [],
  plugins: [
    ["babel-plugin-root-import", {"rootPathPrefix": "~"}],
    ["@babel/plugin-syntax-dynamic-import"],
    ['@babel/plugin-proposal-optional-chaining']
  ],
  env: {
    test: {
      plugins: [],
      presets: [["env", { "targets": { "node": "current" } }]]
    }
  }
};
