import { eventBus } from '../../main'

export default {
  bind(el, binding) {
    el.addEventListener('click', function(event) {
      if (binding.value && typeof binding.value === 'function') {
        binding.value();
      }
      eventBus.$emit('stop-propagated-click');
      event.stopPropagation();
    })
  },
  unbind(el, binding) {
    el.removeEventListener('click', function(event) {
      if (binding.value && typeof binding.value === 'function') {
        binding.value();
      }
      eventBus.$emit('stop-propagated-click');
      event.stopPropagation();
    });
  }
};