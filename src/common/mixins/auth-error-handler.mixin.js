import { HTTP } from '@/common/services/http.service'
import { COMMENTS } from '@/stores/actions/comments.type';
import { formatDistanceToNow } from 'date-fns'

export const authErrorHandlerMixin = {
  methods: {
    $_authErrorHandlerMixin_handle(error) {
      return error.response?.data?.error ? error.response.data.error : 'Failed to authorize'
    },
  }
};