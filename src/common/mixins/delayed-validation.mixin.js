const touchMap = new WeakMap();

export const delayedValidationMixin = {
  methods: {
    $_delayedValidationMixin_validate($v) {
      $v.$reset();

      if (touchMap.has($v)) {
        clearTimeout(touchMap.get($v))
      }

      touchMap.set($v, setTimeout($v.$touch, 1000))
    },
  }
};