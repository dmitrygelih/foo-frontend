export const lengthCheckMixin = {
  methods: {
    $_lengthCheckMixin_inBetween(text, minLength, maxLength) {
      const textLength = text.length;
      return minLength <= textLength && textLength <= maxLength;
    },
  }
};