export const publicPathMixin = {
  data() {
    return {
      $_publicPathMixin_path: process.env.BASE_URL
    }
  }
};