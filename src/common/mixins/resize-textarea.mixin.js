export const resizeTextAreaMixin = {
  methods: {
    $_resizeTextAreaMixin_resize(textArea) {
      textArea.style.height = "auto";
      textArea.style.height = `${textArea.scrollHeight}px`;
    }
  }
};