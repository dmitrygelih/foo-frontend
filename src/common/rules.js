import { AbilityBuilder } from '@casl/ability'

export default function defineRulesFor(user) {
  const { can, rules } = new AbilityBuilder();
  
  can('manage', ['Post', 'Comment'], { 'author.id': user.id });
  can('manage', 'User', { id: user.id });
  
  return rules
}