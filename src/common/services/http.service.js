import axios from 'axios'
import store from '@/stores'
import router from '@/router'
import { AUTH } from "@/stores/actions/auth.type"
import { MODAL } from "@/stores/actions/modal.type"
import { ERROR } from "@/stores/actions/error.type"
import { JWTService } from "~/src/common/services/jwt.service";

const LOGIN_PATH = '/login';
const SIGNUP_PATH = '/registration';

const HTTP = axios.create({
  baseURL: 'http://0.0.0.0:3000'
});

HTTP.interceptors.request.use(config => {
  const token = JWTService.getToken();
  
  if (token)
    config.headers.Authorization = `Bearer ${token}`;
  
  return config;
}, err => {
  return Promise.reject(err);
});

HTTP.interceptors.response.use(
  (response) => { return response },
  (error) => {
    if (router.currentRoute.path === LOGIN_PATH || router.currentRoute.path === SIGNUP_PATH)
      return Promise.reject(error);
    
    switch (error.response?.status) {
      case 404: router.replace({ name: 'NonExistent' }); break;
      case 401: store.commit(MODAL.UNAUTHORIZED_DIALOG.SHOW); return Promise.reject(error);
    }
    
    store.commit(ERROR.SHOW, error);
    return Promise.reject(error)
  }
);

export { HTTP }
