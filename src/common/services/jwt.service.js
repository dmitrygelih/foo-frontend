const ID_TOKEN_KEY = "user-token";

export const JWTService = {
  getToken() {
    return window.localStorage.getItem(ID_TOKEN_KEY);
  },
  
  saveToken(token) {
    window.localStorage.setItem(ID_TOKEN_KEY, token);
  },

  removeToken() {
    window.localStorage.removeItem(ID_TOKEN_KEY);
  }
};
