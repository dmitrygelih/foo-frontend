export const TYPE_KEY = Symbol('resourceType');

export function typeAs(type) {
  return (item) => {
    item[TYPE_KEY] = type;
    return item
  }
}

export const subjectTypeDefiner = {
  subjectName: (subject) => {
    return !subject || typeof subject === 'string'
      ? subject
      : subject[TYPE_KEY]
  }
};

export function extractAttributeFromObject(attr, obj) {
  const result = obj[attr];

  delete obj[attr];
  
  return result
}