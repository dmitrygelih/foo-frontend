export const noSpaces = (value) => {
  const regEx = /\s/;
  return !regEx.test(value);
};