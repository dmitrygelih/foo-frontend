import Vue from "vue"
import App from "./App.vue"
import router from "../src/router"
import store from "../src/stores"

import 'codemirror/lib/codemirror.css';
import '@toast-ui/editor/dist/toastui-editor-viewer.css';
import '@toast-ui/editor/dist/toastui-editor.css';

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);

import Loading from '@/components/Loading';
Vue.component('loading', Loading);

import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue);

import Notifications from 'vue-notification'
Vue.use(Notifications);

import PictureInput from 'vue-picture-input'
Vue.use(PictureInput);

import VModal from 'vue-js-modal'
Vue.use(VModal, { dialog: true });

import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart, faHeartBroken, faCamera, faUser, faTimes, faTrash, faPencilAlt, faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { faHeart as faRegularHeart } from '@fortawesome/free-regular-svg-icons'

library.add([
  // solid icons
  faHeart, faHeartBroken, faCamera, faUser, faTimes, faTrash, faPencilAlt, faEllipsisV,
  // regular icons
  faRegularHeart
]);

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
Vue.component('font-awesome-icon', FontAwesomeIcon);

import { abilitiesPlugin } from '@casl/vue'
Vue.use(abilitiesPlugin, store.getters.ability);

Vue.config.productionTip = false;
Vue.config.devtools = true;

export const eventBus = new Vue();

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
