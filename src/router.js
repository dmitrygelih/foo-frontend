import Vue from "vue";
import Router from "vue-router";
import store from './stores/index'
import Home from "./views/Home.vue";
import PostSubmit from "./views/post/PostSubmit";

Vue.use(Router);

const LOGIN_ROUTE_NAME = 'Login';
const SIGN_UP_ROUTE_NAME = 'Registration';

const lazyLoad = (view, folder) => {
  if (folder) {
    return () => import(`@/views/${folder}/${view}.vue`)
  }

  return () => import(`@/views/${view}.vue`)
};

const redirectIfAuthenticated = (to, from, next) => {
  store.getters.isAuthenticated ? next({ name: 'Home', params: { page: 1 } }) : next()
};

const isAuthRoute = (routeName) => {
  return routeName === LOGIN_ROUTE_NAME || routeName === SIGN_UP_ROUTE_NAME;
};


const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      name: LOGIN_ROUTE_NAME,
      component: lazyLoad('Login', 'auth'),
      beforeEnter: redirectIfAuthenticated
    },
    {
      path: "/registration",
      name: SIGN_UP_ROUTE_NAME,
      component: lazyLoad('Registration', 'auth'),
      beforeEnter: redirectIfAuthenticated
    },
    {
      path: '/home',
      redirect: { name: 'Home' }
    },
    {
      path: "/home/:page",
      beforeEnter: (to, from, next) => {
        Number(to.params.page) ? next() : next({ name: 'Home', params: { page: 1 } })
      },
      name: "Home",
      component: Home
    },
    {
      path: "/post/submit",
      name: "PostSubmit",
      component: PostSubmit
    },
    {
      path: "/posts/:id/show",
      name: "PostShow",
      component: lazyLoad("PostShow", "post"),
      props: true
    },
    {
      path: "/posts/:id/edit",
      name: "PostEdit",
      component: lazyLoad("PostEdit", "post"),
      props: true,
    },
    {
      path: "/users/:id",
      component: lazyLoad("User", "user"),
      name: 'User',
      redirect: '/users/:id/general',
      children: [
        {
          path: 'general',
          name: 'UserGeneral',
          component: lazyLoad("UserGeneral", "user")
        },
        {
          path: 'posts',
          name: 'UserPosts',
          component: lazyLoad("UserPosts", "user"),
        },
        {
          path: 'comments',
          name: 'UserComments',
          component: lazyLoad("UserComments", "user"),
        },
      ]
    },
    {
      path: '/404',
      name: '404',
      component: lazyLoad("NotFound", "error")
    },
    {
      path: '/non-existent',
      name: 'NonExistent',
      component: lazyLoad("NonExistent", "error")
    },
    {
      path: "*",
      redirect: to => { return '/404' }
    }
  ]
});

router.beforeEach((to, from, next) => {
  (!isAuthRoute(to.name) && !store.getters.isAuthenticated) ? next({ name: LOGIN_ROUTE_NAME }) : next()
});

export default router
