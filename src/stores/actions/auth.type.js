export const AUTH = {
  // actions
  LOGIN: 'AUTH_LOGIN',
  SIGN_UP: 'AUTH_SIGN_UP',
  LOGOUT: 'AUTH_LOGOUT',
  // mutations
  TOKEN: {
    SAVE: 'AUTH_TOKEN_SAVE',
    PURGE: 'AUTH_TOKEN_PURGE'
  },
  RULES: {
    CLEAR: 'AUTH_RULES_CLEAR',
    DEFINE: 'AUTH_RULES_DEFINE'
  },
};
