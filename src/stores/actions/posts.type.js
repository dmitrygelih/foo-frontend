export const POSTS = {
  // actions
  FETCH: 'POSTS_FETCH',
  REMOVE: 'POST_REMOVE',
  USER_POSTS: 'USER_POSTS',
  // mutations
  STORE: 'POSTS_STORE',
  CLEAR: 'POSTS_CLEAR',
  DELETE: 'POSTS_DELETE'
};
