export const USER = {
  // actions
  FETCH: 'USER_FETCH',
  UPDATE: 'USER_UPDATE',
  AVATAR: {
    UPLOAD: 'AVATAR_UPLOAD',
    REMOVE: 'AVATAR_REMOVE',
    SAVE: 'AVATAR_SAVE'
  },
  // mutations
  SAVE: 'USER_SAVE',
  PURGE: 'USER_PURGE',
};
