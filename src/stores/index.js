import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import user from './modules/user'
import comments from './modules/comments'
import error from './modules/error'
import posts from './modules/posts'
import modal from './modules/modal'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex);

export default new Vuex.Store({
  strict: false,
  modules: {
    auth,
    user,
    comments,
    posts,
    modal,
    error
  },
  plugins: [
    createPersistedState({
      paths: ['auth', 'user'],
    })
  ],
})
