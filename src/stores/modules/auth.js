import store from '@/stores';
import { Ability } from '@casl/ability'
import { TYPE_KEY, subjectTypeDefiner } from '@/common/utils'
import { HTTP } from '@/common/services/http.service'
import { AUTH } from '@/stores/actions/auth.type'
import { USER } from '@/stores/actions/user.type'
import { JWTService } from "@/common/services/jwt.service";
import defineRulesFor from '@/common/rules';

function getTokenFromHeaders(headers) {
  return headers.authorization.split(' ')[1]
}

function authRequest(route, requestParams) {
  return new Promise((resolve, reject) => {
    HTTP.post(route, requestParams)
      .then(response => {
        store.commit(AUTH.TOKEN.SAVE, getTokenFromHeaders(response.headers));
        store.commit(USER.SAVE, response.data.user);

        resolve(response)
      })
      .catch(error => {
        store.commit(AUTH.TOKEN.PURGE);

        reject(error)
      })
  })
}


const state = {
  isAuthenticated: !!JWTService.getToken(),
  rules: []
};

const getters = {
  ability(state) {
    return new Ability(state.rules, subjectTypeDefiner)
  },
  isAuthenticated(state) {
    return state.isAuthenticated
  }
};

const actions = {
  [AUTH.SIGN_UP]: ({}, params) => {
    return authRequest('/signup', params);
  },
  [AUTH.LOGIN]: ({}, params) => {
    return authRequest('/login', params)
  },
  [AUTH.LOGOUT]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      HTTP.delete('/logout')
        .then(response => resolve(response))
        .catch(error => reject(error))
        .finally(() => {
          commit(AUTH.TOKEN.PURGE);
          commit(USER.PURGE);
        });
    })
  }
};

const mutations = {
  [AUTH.TOKEN.SAVE]: (state, token) => {
    JWTService.saveToken(token);
    state.isAuthenticated = true;
  },
  [AUTH.TOKEN.PURGE]: (state) => {
    JWTService.removeToken();
    state.isAuthenticated = false;
  },
  [AUTH.RULES.DEFINE]: (state, params) => {
    state.rules = defineRulesFor(params.user);
    params.ability.update(state.rules)
  },
  [AUTH.RULES.CLEAR]: (state, ability) => {
    return new Promise(resolve => {
      state.rules = [];
      ability.update(state.rules);

      resolve()
    })
    ;
  },
};

export default {
  state,
  actions,
  getters,
  mutations
};
