import Vue from 'vue'
import { COMMENTS } from '../../stores/actions/comments.type'
import { HTTP } from '~/src/common/services/http.service'
import { typeAs } from "../../common/utils";

const state = {
  list: []
};

const getters = {
  find(id) {
    return state.list.find(comm => { return comm.id === id } );
  },
  areCommentsPresent() {
    return !!state.list.length
  }
};

const actions = {
  [COMMENTS.REQUEST.USER_COMMENTS]: ({ commit }, username) => {
    return new Promise((resolve, reject) => {
      HTTP.get(`/users/${username}/comments`)
        .then(response => {
          commit(COMMENTS.STORE, response.data.comments);
          resolve(response)
        })
        .catch(error => reject(error))
    });
  },
  [COMMENTS.REQUEST.CREATE]: ({ commit }, params) => {
    return new Promise((resolve, reject) => {
      HTTP.post('/comments', params)
        .then(response => {
          commit(COMMENTS.UNSHIFT, response.data.comment);
          resolve(response)
        })
        .catch(error => reject(error))
    });
  },
  [COMMENTS.REQUEST.UPDATE]: ({ commit }, comment) => {
    return new Promise((resolve, reject) => {
      HTTP.patch(`/comments/${comment.id}`, { text: comment.text })
        .then(response => {
          commit(COMMENTS.UPDATE, {
            id: comment.id,
            text: comment.text
          });
          resolve(response)
        })
        .catch(error => reject(error))
    })
  },
  [COMMENTS.REQUEST.REMOVE]: ({ commit }, id) => {
    return new Promise((resolve, reject) => {
      HTTP.delete(`/comments/${id}`)
        .then(response => {
          commit(COMMENTS.REMOVE, id);
          resolve(response)
        })
        .catch(error => reject(error))
    })
  },
};

const mutations = {
  [COMMENTS.STORE]: (state, comments) => {
    Vue.set(state, 'list', [...comments.map(typeAs('Comment'))]);
  },
  [COMMENTS.UNSHIFT]: (state, comment) => {
    state.list.unshift(typeAs('Comment')(comment))
  },
  [COMMENTS.REMOVE]: (state, id) => {
    state.list.find((comment, index) => {
      if (comment.id === id) {
        return state.list.splice(index, 1)
      }
    })
  },
  [COMMENTS.UPDATE]: (state, params) => {
    const comment = getters.find(params.id);
    comment.text = params.text
  },
  [COMMENTS.CLEAR]: (state) => {
    Vue.set(state, 'list', []);
  }
};

export default {
  state,
  getters,
  actions,
  mutations,
}