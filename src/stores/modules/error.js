import Vue from 'vue'
import { ERROR } from '~/src/stores/actions/error.type'

const state = {};

const getters = {};

const actions = {};

const mutations = {
  [ERROR.CLEAN]: () => {
    Vue.notify({ clean: true })
  },
  [ERROR.SHOW]: ({}, error) => {
    Vue.notify({
      type: 'error',
      title: error.response?.data?.title || 'Something went wrong',
      text: error.response?.data?.error || error.message
    });
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
