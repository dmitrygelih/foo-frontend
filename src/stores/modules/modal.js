import { MODAL } from '~/src/stores/actions/modal.type'

const state = {
  display: false,
};

const getters = {};

const actions = {};

const mutations = {
  [MODAL.UNAUTHORIZED_DIALOG.SHOW]: (state) => {
    state.display = true;
  },
  [MODAL.UNAUTHORIZED_DIALOG.HIDE]: () => {
    state.display = false;
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
