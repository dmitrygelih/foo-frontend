import Vue from 'vue'
import { POSTS } from '@/stores/actions/posts.type'
import { typeAs } from "@/common/utils";
import { HTTP } from '~/src/common/services/http.service'

const state = {
  list: []
};

const getters = {
  arePostsPresent() {
    return !!state.list.length
  }
};

const actions = {
  [POSTS.FETCH]: ({ commit }, params) => {
    return new Promise((resolve, reject) => {
      HTTP.get('/posts', params)
        .then((response) => {
          commit(POSTS.STORE, response.data.posts);
          resolve(response);
        })
        .catch(error => reject(error))
    });
  },
  [POSTS.REMOVE]: ({ commit }, id) => {
    return new Promise((resolve, reject) => {
      HTTP.delete('/posts/' + id)
        .then(response => {
          commit(POSTS.DELETE, id);
          resolve(response)
        })
        .catch(error => reject(error))
    });
  },
  [POSTS.USER_POSTS]: ({ commit }, name) => {
    return new Promise((resolve, reject) => {
      HTTP.get(`/users/${name}/posts`)
        .then((response) => {
          commit(POSTS.STORE, response.data.posts);
          resolve(response)
        })
        .catch(error => reject(error))
    });
  },
};

const mutations = {
  [POSTS.STORE]: (state, posts) => {
    Vue.set(state, 'list', [...posts.map(typeAs('Post'))])
  },
  [POSTS.CLEAR]: (state) => {
    Vue.set(state, 'list', []);
  },
  [POSTS.DELETE]: (state, id) => {
    state.list.find((post, index) => {
      if (post.id === id) {
        return state.list.splice(index, 1)
      }
    })
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
}