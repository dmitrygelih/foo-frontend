import { HTTP } from '~/src/common/services/http.service'
import { extractAttributeFromObject } from '~/src/common/utils'
import { USER } from '~/src/stores/actions/user.type'

const getDefaultState = () => {
  return {
    id: null,
    email: "",
    avatarUrl: "",
  }
};


const getters = {};

const state = getDefaultState();

const actions = {
  [USER.AVATAR.UPLOAD]: ({ commit, state }, params) => {
    return new Promise((resolve, reject) => {
      HTTP.post(`/users/${state.id}/upload_avatar`, params)
        .then(response => {
          commit(USER.AVATAR.SAVE, response.data.avatarUrl);
          resolve(response);
        })
        .catch(error => reject(error))
    });
  },
  [USER.AVATAR.REMOVE]: ({ commit, state }) => {
    return new Promise((resolve, reject) => {
      HTTP.post(`/users/${state.id}/remove_avatar`)
        .then(response => {
          commit(USER.AVATAR.REMOVE);
          resolve(response);
        })
        .catch(error => reject(error))
    });
  },
  [USER.FETCH]: ({}, id) => {
    return new Promise((resolve, reject) => {
      HTTP.get(`/users/${id}`)
        .then(response => resolve(response) )
        .catch(error => reject(error))
    })
  },
  [USER.UPDATE]: ({ commit, state }, params) => {
    return new Promise((resolve, reject) => {
      HTTP.patch(`/users/${state.id}`, params)
        .then((response) => {
          commit(USER.SAVE, params);
          resolve(response)
        })
        .catch(error => reject(error))
    })
  }
};

const mutations = {
  [USER.SAVE]: (state, user) => {
    Object.assign(state, user)
  },
  [USER.PURGE]: (state) => {
    Object.assign(state, getDefaultState());
  },
  [USER.AVATAR.SAVE]: (state, avatarUrl) => {
    state.avatarUrl = avatarUrl
  },
  [USER.AVATAR.REMOVE]: (state) => {
    Object.assign(state, { avatarUrl: '' })
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
}